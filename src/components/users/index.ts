import { CONN } from '../../instances'
import { ServerError } from '../../error';
import { User } from './modelo';
import { LANG, CONFIG } from './../../config';
import { createTransport } from 'nodemailer';
import { Notifications } from '../notifications';
import * as Mail from 'nodemailer/lib/mailer'
import * as crypto from 'crypto';
import * as _ from 'lodash';
import * as SQL from './queries';
import * as Utils from '../../dependencies/utils';


const SECRET_EXPIRE_SECONDS = 60 * 15;
const MAX_RESETS_PER_WINDOW = 10;
const RESET_WINDOW_SECONDS = 24*60*60;
const MAX_SECRET_GUESSES = 10;

export class Users {
	tools = [
		{ name: 'congress', url: '/congreso' },
		{ name: 'tactics', url: '/tacticas' },
		{ name: 'medios', url: '/medios' },
		{ name: 'sector', url: '/sector' },
		{ name: 'academia', url: '/academia' },
	];
	constructor() {

	}

	static validarClave = (clave: string, hash: string): boolean => {
		let [digest, iteraciones, sal, pbkdf2] = hash.split(':')
		let pbkdf2Buffer = Buffer.from(pbkdf2, 'base64');

		return pbkdf2Buffer.equals(crypto.pbkdf2Sync(clave, Buffer.from(sal, 'base64'), Number(iteraciones), pbkdf2Buffer.length, digest))
	}

	static crearClave = (clave: string): string => {
		let sal = crypto.randomBytes(32);
		let digest = 'sha256';
		let iteraciones = 1000;
		let hash = crypto.pbkdf2Sync(clave, sal, iteraciones, 32, digest);

		return `${digest}:${iteraciones}:${sal.toString('base64')}:${hash.toString('base64')}`;
	}

	_login = (params: { username: string, password: string }, request): Promise<User> => {
		if (typeof params.username !== "string") return Promise.resolve({} as User); //throw new ServerError("username not valid", 400);
		if (typeof params.password !== "string") return Promise.resolve({} as User); //throw new ServerError("password not valid", 400);
		let username: string = params.username;
		let password: string = params.password;

		return CONN.ejecutarQueryPreparado('SELECT u.*, c.services FROM _user u JOIN _client c ON c.id=u.id_client WHERE u.username = :username',
			{
				username: username
			}).then((users: User[]) => {

				if (!users.length) throw new ServerError("login_failed", 403);
				if (!Users.validarClave(password, users[0].password)) throw new ServerError("login_failed", 403);
				let user = users[0];
				user['services'] = this.tools.filter((el, idx) => user['services'].toString(2).split('')[idx])
				
				request.session.user = users[0];
				request.session.save()
				delete users[0].password;
				return users[0];
			});
	}

	_isLogged = (params: null, request): boolean => {
		return typeof request.session.user !== 'undefined';
	}

	_logout = (params: null, request): boolean => {
		request.session.destroy();
		return true;
	}

	_get = (params: null, request): User => request.session.user;

	_set = (params: { user: User }, request): Promise<User> => {

		let nuevoUsuario: User = {
			username: params.user.username,
			password: Users.crearClave(params.user.password),
			id_client: +params.user.id_client,
			name: params.user.name,
			role: params.user.role,
			language: params.user.language
		}
		return CONN.insertar('_user', [nuevoUsuario]).then(() => {
			return nuevoUsuario;
		})
	}

	_changeLang = (params: { lang: string }, request): Promise<boolean> => {
		if (Object.keys(LANG).indexOf(params.lang) == -1)
			throw new ServerError('invalid_language', 400);

		return CONN.actualizar('_user', { language: params.lang }, { username: request.session.user.username }).then(res => {
			request.session.user.language = params.lang;
			return true;
		}).catch(() => {
			return false;
		})
	}

	_changePass = (params: { old_password: string, new_password: string }, request) => {
		let username = request.session.user.username;
		return CONN.leer('_user', undefined, { username: { valor: username, operador: '=' } }).then((users: User[]) => {
			if (!Users.validarClave(params.old_password, users[0].password))
				return false;
			return CONN.actualizar('_user', { password: Users.crearClave(params.new_password) }, { id: request.session.user.id }).then(res => true)
		})
	}

    _signIn = async (params:{user: {username:string,password:string, subject: string}, device: any}, request) => {
		if (!params.user) throw new ServerError('user invalid', 400);
		if (typeof params.user.username !== 'string' || !Utils.isValidEmail(params.user.username)) throw new ServerError('username invalid', 400);
        if (typeof params.user.password !== 'string' || params.user.password.length < 6 ) throw new ServerError('password invalid', 400);
		if (typeof params.user.subject !== 'string') throw new ServerError('subject invalid', 400);
		if (typeof params.device.id !== "string") throw new ServerError('device.id not valid', 400);
		if (typeof params.device.manufacturer !== "string") throw new ServerError('device.manufacturer not valid', 400);
		if (typeof params.device.model !== "string") throw new ServerError('device.model not valid', 400);
		if (typeof params.device.os_version !== "string") throw new ServerError('device.os_version not valid', 400);
		if (typeof params.device.app_version !== "string") throw new ServerError('device.app_version not valid', 400);


        let client: number;
        if (params.user.subject == 'politics') {
            client = 4;
        } else if (params.user.subject == 'sports') {
            client = 3;
        } else {
            throw new ServerError('subject invalid', 400);
        }

        let result = await CONN.insertar('_user', [{
            username: params.user.username,
            name: params.user.username,
            password: Users.crearClave(params.user.password),
            role: 1,
            id_client: client,
            language: 'ES'
		}]);
		let idUser;
		if (result && result.insertId) {
			idUser = result.insertId;
		}

		result = await CONN.insertar('_device', [{
			id: params.device.id,
			manufacturer: params.device.manufacturer,
			model: params.device.model,
			os_version: params.device.os_version,
			registered: new Date(),
			app_version: params.device.app_version,
			token_expired: 1
		}], false);
		await CONN.insertar('_device_user', [{
			id_user: idUser,
			id_device: params.device.id
		}]);
        return true;
    } 

	_setResetSecret = async (params: { mail: string }, request) => {
		if (typeof params.mail !== "string") throw new ServerError('mail not valid', 400);
		let app: 'ghai'|'wec' = request.get('Content-Type').match(/\/wec\//) ? 'wec' : 'ghai';

		let savedSecret = await CONN.ejecutarQueryPreparado(SQL.GET_SECRET, {
			mail: params.mail
		});
		let now = Math.floor((+new Date()) / 1000);
		if (savedSecret.length > 0 && (savedSecret[0].used)) {
			await CONN.eliminar('_user_secret', {id: savedSecret[0].id});
			savedSecret = [];
		} 
		if (savedSecret.length > 0 && (savedSecret[0].generated + SECRET_EXPIRE_SECONDS) < now) {
			let resetsLastDay = 0;
			if (savedSecret[0].generated + RESET_WINDOW_SECONDS < now) {
				resetsLastDay = savedSecret[0].resets + 1
			}
			if (savedSecret[0].resets > MAX_RESETS_PER_WINDOW) {
				return;
			}
			
			let secretFields = savedSecret[0];
			secretFields.secret = crypto.randomBytes(48).toString('hex').substr(0, 10);
			secretFields.generated = now;
			secretFields.resets = resetsLastDay;
			await CONN.insertar('_user_secret', [{
				id: secretFields.id,
				secret: secretFields.secret,
				generated: secretFields.generated,
				resets: secretFields.resets,
				guesses: 0,
				used: null
			}], true);
			this.sendMail(secretFields, app);
		}
		
		return true;
	}

	private sendMail(userInfo: {id: string, username: string, name: string, language: string, secret: string}, app: 'ghai'|'wec') {
		let message: {subject: string, text: string};
		//El link se usa para llenar los campos de mail y secred del formulario de cambio de clave automáticamente
		let link = 'https://iinsightapp.com/' + app + '/finish_password_reset/' + Buffer.from(JSON.stringify({
			mail: userInfo.username, 
			secret: userInfo.secret
		})).toString('base64');
		let appName = 'Insight';
		if (userInfo.language == 'ES') {
			message = {
				subject: "Recuperación de contraseña para " + appName,
				text: "Alguien ha solicitado una recuperación de contraseña para su cuenta de " + appName + ". " 
				+ "Para completar el proceso utilice el código de recuperación \n\n" 
				+ userInfo.secret 
				+ "\n\n" + "o siga el siguiente enlace \n\n" 
				+ link
				+ "\n\n " + "Si no solicitó un cambio de contraseña puede ignorar este mensaje. La contraseña "
				+ "no cambiará hasta que cree una nueva contraseña."
				+ "\n\nEste enlace y código expirarán después de un día de ser generados y sólo pueden ser usados una vez."

			}
		} else {
			message = {
				subject: "Password reset request for " + appName,
				text: "Someone requested that the password for your " + appName 
				+ " account be reset. " 
				+ "To reset your password use the code \n\n" 
				+ userInfo.secret + "\n\n" + "or follow this link  " + link 
				+ "\n\n " + "If you didn't request this, you can ignore this email. The password "
				+ "won't change until you create a new password."
				+ "\n\nThis link and code will expire one day after being generated and can only be used once."
			}
		}
		console.log({message});
		
		
		let transporter = createTransport({
			sendmail: true,
			newline: 'unix',
			path: '/usr/sbin/sendmail'
		});
		let sendMail = new Promise((resolve, reject) => {
			let mailOptions: Mail.Options = {
				from: 'Soporte noreply@iinsightapp.com',
				to: userInfo.name + ' ' + userInfo.username,		
				subject: message.subject,
				text: message.text,
			};

			transporter.sendMail(mailOptions, (error, info) => {
				if (error) {
					reject(error);
				} else {
					resolve(info);
				}
			});
		});
		sendMail.then(res => {
			if (CONFIG['debug']) {
				console.log('sendMail completed')
				console.log(res)
			}
		}).catch(err => {
			if (CONFIG['debug']) {
				console.log('sendMail failed')
				console.log(err)
			}
		});
		return sendMail;
	}

	_resetPassword = async (params: { secret: string, mail: string, password: string}, request) => {
		if (typeof params.secret !== "string") throw new ServerError('secret not valid', 400);
		if (typeof params.mail !== "string") throw new ServerError('mail not valid', 400);
		if (typeof params.password !== "string") throw new ServerError('password not valid', 400);

		let savedSecret = await CONN.ejecutarQueryPreparado(SQL.GET_SECRET, {
			mail: params.mail
		}) as {id: number, secret: string, generated: number, guesses: number, used: number}[];

		let now = Math.floor((+new Date()) / 1000);
		let status = {
			invalid: false,
			blocked: false,
			expired: false
		};

		if (savedSecret.length == 0) {
			status.invalid = true;
		} else if (savedSecret[0].guesses > MAX_SECRET_GUESSES) {
			status.blocked = true;
		} else if (savedSecret[0].secret !== params.secret) {
			status.invalid = true;
		} else if (
			savedSecret[0].secret === params.secret  && 
			((savedSecret[0].generated + SECRET_EXPIRE_SECONDS) < now || 
			(savedSecret[0].used))
		) {
			status.expired = true;
		}

		if (!status.invalid && !status.blocked && !status.expired) {
			let result = await CONN.ejecutarQueryPreparado(`UPDATE _user SET password = :new_password WHERE id = :id_user LIMIT 1`, {
				id_user: Number(savedSecret[0].id),
				new_password: Users.crearClave(params.password)
			});
			await CONN.actualizar('_user_secret', {
				used: Math.floor((+new Date()) / 1000)
			}, {
				id: savedSecret[0].id
			});
		} else {
			if (savedSecret[0]) {
				await CONN.actualizar('_user_secret', {
					guesses: savedSecret[0].guesses + 1
				}, {
					id: savedSecret[0].id 
				});
			} else {
				status.invalid = true;
			}
		}
		
		return status;
	}

}
