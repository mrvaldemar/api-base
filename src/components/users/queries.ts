export const GET_SECRET = `SELECT
_user.id,
_user.username,
_user.name,
_user.language,
_user_secret.secret,
_user_secret.generated, 
_user_secret.resets,
_user_secret.guesses ,
_user_secret.used
FROM _user
LEFT JOIN _user_secret ON _user_secret.id = _user.id
WHERE _user.username = :mail`;