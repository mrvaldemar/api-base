export interface User {
	id?: number,
	username: string,
	name: string,
	password: string,
	role: number,
	id_client: number,
	language: string
}