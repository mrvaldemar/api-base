require('source-map-support').install();

import { CONFIG } from './config';
import { Services, queryCount } from './dependencies';
import { CONN } from './instances';
import { ServerError } from './error';
import * as arg from 'arg';

import * as express from 'express';
import * as session from 'express-session';
import * as bodyParser from 'body-parser';
import * as multer from 'multer';
import * as uuid from 'uuid/v4';
import * as fs from 'fs';

const args = arg({
	'--port': Number,
	'-p': '--port'
});

if (args['--port'] !== undefined) {
	CONFIG['puerto'] = args['--port'];
}


const app = express();
const puerto = CONFIG['puerto'];
const dominio = CONFIG['dominio'];


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

var upload = multer({ dest: `${CONFIG['root']}/temp/` })

app.use(session({
	genid: req => {
		if (req.body.sessid) {
			return req.body.sessid;
		} else {
			return uuid();
		}
	},
	secret: '8iKiCZhH47XHp1vH',
	resave: true,
	saveUninitialized: true,
	cookie: { maxAge: 31536000000 }
}));

app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	next();
});

app.post("/fileupload", upload.single('fileKey'), async (req, res) => {
	let file = req.file;

	fs.exists(file.path, exists => {
		let datos = {};

		if (exists) {

			let extension = file.originalname.split('.')[file.originalname.split('.').length - 1]
			let filename = `${uuid().slice(0, 8)}.${extension}`;

			let fileRegister = {
				'filename': filename,
				'og_filename': file.originalname,
				'filesize': file.size
			}

			fs.copyFileSync(file.path, `${CONFIG['root']}/files/${filename}`);

			CONN.insertar('file', [fileRegister]).then(() => {

				datos['success'] = true;
				res.send(datos);

			}).catch(err => {

				datos['success'] = false;
				datos['error'] = err.message;
				datos['code'] = 500;
				res.send(datos);
			})

		} else {
			datos['success'] = false;
			datos['error'] = 'file_not_exists';
			res.send(datos);
		}
	})
})
app.get("/getsid", async (req, res) => {
	res.send(req.sessionID)
})

app.post("/", async (req, res) => {
	let datos = {};
	let parametros = Services.getRequest(req);
	let time = +new Date();
	console.time(parametros['action'] + (time));

	Services.execute(parametros, req).then(respuesta => {
		console.timeEnd(parametros['action'] + (time));
		datos['data'] = respuesta;
		datos['success'] = true;

		if (CONFIG['debug'])
			datos['query_count'] = queryCount;

		res.send(datos);

	}).catch(err => {
		console.timeEnd(parametros['action'] + (time));
		datos['success'] = false;
		datos['error'] = err.message;
		if (CONFIG['debug']) {
			datos['stacktrace'] = err.stack;
			datos['query_count'] = queryCount;
		}

		if (err instanceof ServerError) {
			datos['code'] = err.codigo;
		}
		if (CONFIG['debug']) {
			console.log(datos);
		}

		res.send(datos);
	})
});
// console.log(CONFIG['root'] + 'files');

app.use('/files', express.static(CONFIG['root'] + '/files'));

CONN.createPool().then(() => {

	app.listen(puerto, () => {
		console.log(`Servidor publicado en: [${dominio}:${puerto}]`);
	});
}).catch(err => {
	console.log({
		respuesta: 'No hay conexion con la base de datos',
		estado: 'ERROR',
		codigo: 500
	})

})