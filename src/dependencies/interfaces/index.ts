export interface NetworkGraphItem {
	[idx: number]: string;
}

export interface ComplyFunction<T> {
	(agent: T, value: any): boolean
}


export enum Affinity {
	opposing = 1,
	neutral = 2,
	allies = 3,
}

export interface DateRange {
	start_date: Date | string,
	end_date: Date | string
}

export interface Pagination {
	page: number,
	items?: number
}

export interface Order {
	field: string,
	direction: 'ASC' | 'DESC'
}

export interface CloudItem {
	word: string,
	font?: number,
	opacity?: number,
	count: number
}

export interface HozBarGraphItem {
	name: string,
	y: number
}

export interface ISFile {
	id: number,
	filename: string,
	og_filename: string,
	filesize: number
}

export interface ComponentResponse<DataType> {
	data: DataType[],
	count: number
}
