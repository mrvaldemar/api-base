
export let GET_FORM = `SELECT
fl.id as field_id,
fl.name as field_name,
fl.relation as field_relation,
fl.type as field_type,
fl.order as field_order,
fl.id_register as field_id_register,
fl.language as field_language,
fl.required as field_required,
fl.label as field_label,
f.id as form_id,
f.name as form_name,
r.id as register_id,
r.table as register_table,
r.group as register_group,
r.required as register_required,
r.order as register_order,
r.dependencies as register_dependencies
FROM _form f
JOIN _register r ON r.id_form = f.id
JOIN _field_view fl ON fl.id_register = r.id
WHERE (fl.language = 1 OR fl.language is null)
AND f.name=:form
order by f.id,r.order,fl.order`;