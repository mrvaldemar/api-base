import { CONN } from '../../instances';
import { CONFIG, LANG } from './../../config';

import * as CACHE from './../../dependencies/cache';
import * as Utils from '../utils';
import * as SQL from './queries';
import * as _ from 'lodash';

export class Insert {
	constructor() { }


	static getStructure<T>(object: T): string[] {
		let res: string[] = [];
		if (Utils.isPrimitive(object))
			return [''];

		for (const key in object) {

			if (Utils.isPrimitive(object[key])) {
				res.push(key);
			} else if (Array.isArray(object[key])) {
				if (Utils.isPrimitive(object[key][0])) {
					res = res.concat([`[[${key}]]`]);
				} else {
					res = res.concat(Insert.getStructure(object[key][0]).map(item => `[[${key}]].${item}`));
				}
			} else {
				res = res.concat(Insert.getStructure(object[key]).map(item => `{{${key}}}.${item}`));
			}
		}

		return res;
	}

	static getValues(object: any) {
		let res = {};

		for (const key in object) {

			if (Utils.isPrimitive(object[key])) {
				res[key] = object[key];
			} else if (Array.isArray(object[key])) {
				if (Utils.isPrimitive(object[key][0])) {
					res[`${key}`] = object[key];
				} else {
					res[`${key}`] = Object.values(Insert.getValues(object[key]));
				}
			} else if (object[key] !== null) {
				if (!!Object.values(object[key]).filter(el => Array.isArray(el)).length) {
					res[`${key}`] = object[key]

				} else {
					_.each(Object.keys(Insert.getValues(object[key])), el => {
						res[`{{${key}}}.${el}`] = object[key][el];
					})
				}

			}
		}

		return res;
	}

	static getDependencyTree(formName: string, lang: number) {
		return CONN.ejecutarQueryPreparado(SQL.GET_FORM, { language: lang, form: formName }).then(res => {
			let registers = res.map(item => Utils.extractSubobject(item, 'register_'))
				.reduce((obj, register) => {
					obj[register['id']] = register;
					return obj;
				}, {});

			let tree = {};
			// return registers;
			_.each(registers, register => {
				if (register['dependencies']) {

					tree[registers[register['dependencies']]['table']] = tree[registers[register['dependencies']]['table']] ? tree[registers[register['dependencies']]['table']] : {};
					tree[registers[register['dependencies']]['table']][register['table']] = {};
				} else {
					tree[register['table']] = {}
				}
			})
			return tree;
		});
	}

	static getForm(formName: string, lang: number, idClient: number) {
		return CONN.ejecutarQueryPreparado(SQL.GET_FORM, { language: lang, form: formName }).then(async res => {
			let form: { [x: string]: any; } = {};
			form = res.reduce((obj, field) => {
				obj[field['field_id']] = field;
				return obj;
			}, {})

			for (const key in Utils.filterObj(form, (el) => !!el)) {
				let relation;
				switch (form[key].field_type) {
					case 'selectradio':
						form[key]['options'] = await CONN.leer(form[key]['field_relation'], undefined, { language: lang })
						break;

					case 'select':
					case 'select2':
					case 'select2->select2':
						relation = form[key]['field_relation'] = JSON.parse(form[key]['field_relation']);

						if (Object.keys(relation).indexOf('cache') !== -1) {
							await CACHE.use({
								session: {
									user: {
										language: 'ES',
										id_client: idClient
									}
								}
							}, relation['cache']);
							form[key]['options'] = Object.values(CACHE.data[idClient][lang][relation['cache']]);
							if (Object.keys(relation).indexOf('filter') !== -1) {
								form[key]['options'] = Insert.filterFormOptions(relation['filter'], form[key]['options']);
							}
							form[key]['options'] = form[key]['options'].map(el => {
								return { id: el['id'], name: el['name'] };
							});

						} else {
							form[key]['options'] = await CONN.leer(relation['out'], ['id', 'name'], { language: lang })
							// form[key]['options'] = form[key]['options'].map(el => el['id']);
						}
						break;

				}
			}

			return form;
		})
	}

	static filterFormOptions(filterInfo: any, elements: any) {
		const filters = [];
		const operators: { [operator: string]: (val1: any, val: any) => boolean } = {
			'===': (elementProperty, compareValue) => {
				return elementProperty === compareValue;
			},
			'==': (elementProperty, compareValue) => {
				return elementProperty === compareValue;
			},
			'>=': (elementProperty, compareValue) => {
				return elementProperty >= compareValue;
			},
			'<=': (elementProperty, compareValue) => {
				return elementProperty <= compareValue;
			},
			'!==': (elementProperty, compareValue) => {
				return elementProperty !== compareValue;
			},
			'!=': (elementProperty, compareValue) => {
				return elementProperty != compareValue;
			},
			'contains': (elementProperty, compareValue) => {
				return typeof elementProperty == "string" && typeof compareValue == "string"
					&& elementProperty.toLocaleLowerCase().indexOf(compareValue.toLocaleLowerCase()) > -1;
			}
		};
		const pushFilter = (filter) => {
			if (!filter || !filter.property || typeof filter.value == "undefined") {
				return false;
			}
			if (!operators[filter.operator]) {
				return false;
			}
			filters.push(filter);
			return true;
		};

		//Si es un objeto ponerlo en el listado de filtros
		if (!Array.isArray(filterInfo) && !pushFilter(filterInfo)) {
			if (CONFIG['debug']) {
				console.log("invalid filter");
			}
			return elements;
		}
		//Si es un listado de filtros ponerlos en el listado de filtros
		//dsepués de validarlos
		if (Array.isArray(filterInfo)) {
			filterInfo.forEach(filter => pushFilter(filter));
		}
		return elements.filter(el => {
			//Array#some para detenerse cuando encuentre un true
			//y devolver !pasaFiltro en el callback
			return !filters.some(filter => {
				if (typeof el[filter.property] == "undefined") {
					//ignorar filtro si no existe propiedad
					return false;
				}
				return !operators[filter.operator](el[filter.property], filter.value);
			});
		});
	}


	static async getRegisters<T>(object: T, formName: string, formDefinition: { [x: string]: any; }, session) {
		let lang: number = LANG[session.language];
		let idClient: number = session.id_client;

		return await Insert.getForm(formName, lang, idClient).then(async form => {
			let values = Insert.getValues(object);
			// return values;
			return Utils.extractUniqueVal(form, 'register_table')
				.map(table => Utils.filterObj(form, (arg) => arg['register_table'] === table))
				.filter(table => {
					for (const field in table) {

						if (typeof values[formDefinition[table[field]['field_id']]] == 'undefined'
							&& !table[field]['field_required']
							&& table[field]['register_required']) {
							delete values[formDefinition[table[field]['field_id']]];
							delete table[field];
							continue;
						}

						if (Array.isArray(values[formDefinition[table[field]['field_id']]])) {
							if (values[formDefinition[table[field]['field_id']]].lenght == 0) {
								delete values[formDefinition[table[field]['field_id']]];
								delete table[field];
								continue;
							}
						}

						if (typeof values[formDefinition[table[field]['field_id']]] == 'undefined'
							&& ['sess.client',
								'sess.language',
								'dep.id',
								'sys.date',
								'gen.number',
								'val'].indexOf(table[field]['field_type']) == -1
							&& table[field]['field_name'] !== 'id') {
							return false;
						}
					}
					return true;
				})
				.reduce(async (registerPromise: Promise<any>, registerDef) => {
					let register = await registerPromise;

					if (!Object.values(registerDef).length) return register;
					let registerTable = Object.values(registerDef)[0]['register_table'];

					register[registerTable] = {};

					for (const key in registerDef) {
						let field = registerDef[key];
						let value = null;
						switch (field['field_type']) {
							case 'sess.client':
								value = idClient;
								break;

							case 'sess.language':
								value = lang;
								break;
							case 'dep.id':
								delete register[registerTable][field['field_name']];
								value = '<<ID>>';
								break;
							case 'sys.date':
								let date = new Date();
								value = date;
								break;
							case 'gen.number':
								value = Math.floor(Math.random() * 1E16);
								break;

							case 'file':
								value = await CONN.leer('file', ['id'], {
									og_filename: values[formDefinition[field['field_id']]]
								}).then(res => {
									try {
										return res[0]['id'];
									} catch (e) {
										return null;
									}
								});
								break;



							case 'val':
								value = JSON.parse(field['field_relation'])['val'];
								break;

							case 'select2->select2':
								value = {
									[field['field_name']]: [].concat.apply([], Object.values(values[formDefinition[field['field_id']]])),
									[field['field_relation']['dependence_field']]: [].concat.apply([], Object.keys(values[formDefinition[field['field_id']]])
										.map(k => {
											return values[formDefinition[field['field_id']]][k].map(el => k)
										}))
								};
								if (registerTable == 'event_agent_tactic') {
									value['id_event'] = "<<ID>>";
								} else if (registerTable == 'report_agent_tactic') {
									value['id_report'] = "<<ID>>";
								}

								break;

							default:
								value = values[formDefinition[field['field_id']]];
								break;
						}
						if (typeof register[registerTable][field['field_name']] == 'undefined') {
							if (field['field_type'] == 'select2->select2') {
								register[registerTable] = value
							} else {

								register[registerTable][field['field_name']] = value
							}
						} else {
							register[registerTable][field['field_name']] = [register[registerTable][field['field_name']]];
							register[registerTable][field['field_name']].push(value)
						}
					}
					console.log('insert-register -- formName', formName)
					console.log('insert-register -- lang', lang)
					console.log('insert-register -- idClient', idClient)
					console.log(register)
					return register
				}, Promise.resolve({}))
		})
	}

	static async insert<T>(object: T, formName: string, formDefinition: { [x: string]: any; }, session) {

		let lang: number = LANG[session.language];
		let depTree = await Insert.getDependencyTree(formName, lang);
		let registers = await Insert.getRegisters(object, formName, formDefinition, session);

		let insert = async (tree, regs, depId?) => {
			let tables = Object.keys(tree);
			let insertPromises: Promise<any>[] = [];
			let cleanPromises = depId ? tables.map((table) => {
				let key;
				console.log(table);

				if (!regs[table]) {
					switch (table) {
						case 'event_agent_tactic':
							key = 'id_event';
							break;
						case 'report_agent_tactic':
							key = 'id_report';
							break;
						case 'report_agent':
							key = 'id_report';
							break;
						case 'report_category':
							key = 'id_report';
							break;
						case 'report_tag':
							key = 'id_report';
							break;
						default:
							return [];
					}
				} else if (Object.keys(regs[table]).indexOf('id_client') > -1) {
					console.log("Object.keys(regs[table]).indexOf('id_client') > -1", table);
					return [];
				} else {
					key = Object.keys(regs[table]).filter(k => regs[table][k] == '<<ID>>')[0];
				}
				return CONN.eliminar(table,
					{
						[key]: depId
					})
			}) : [];

			for (let i = 0; i < cleanPromises.length; i++) {
				await cleanPromises[i]
			}

			_.each(tables, table => {
				_.each(regs[table], (field, fieldName) => {
					if (field == '<<ID>>') {
						regs[table][fieldName] = depId;
					} else if (field === null) {
						regs[table][fieldName] = null;
					} else if (Array.isArray(regs[table][fieldName])) {

						let depKey = Object.keys(regs[table]).filter(key => {
							return regs[table][key] == '<<ID>>' || regs[table][key] == depId
						})[0];
						delete regs[table][depKey];
						let valKeys = Object.keys(regs[table]).filter(key => {
							return !(regs[table][key] == '<<ID>>' || regs[table][key] == depId)
						})

						regs[table] = regs[table][valKeys[0]].map((el, i) => {
							return valKeys.reduce((o, k) => {
								o[k] = regs[table][k][i];
								o[depKey] = depId;
								return o;
							}, {})
						});

						return null;
					}
				});

				if (!regs[table]) return;

				if (!regs[table]['id']) {
					delete regs[table]['id'];
				}

				if (regs[table].filter) {
					regs[table] = regs[table].filter(el => el !== null);
				}

				if (regs[table])
					insertPromises.push(CONN.insertar(table, Array.isArray(regs[table]) ? regs[table] : [regs[table]], true).then(res => {
						if (!_.isEmpty(tree[table])) {
							return insert(tree[table], regs, !depId && regs[table]['id'] ? regs[table]['id'] : res.insertId);
						}
						return res;
					}));
			});

			for (let i = 0; i < insertPromises.length; i++) {
				await insertPromises[i].catch(err => {
					console.log(err);
					return err
				});
			}
			return null;
		}

		return insert(depTree, registers)

	}
}