import { LANG, CONFIG } from './../../config';
import * as _ from 'lodash';

export var data: { [key: string]: { [key: string]: any } } = {};



export function get<T>(component: string, id: number, session): T {
	let lang = LANG[session.language];
	let clientId: number = session.id_client;

	return data[clientId][lang][component][id];
}

export function getAll<T>(component: string, session): T[] {
	let lang = LANG[session.language];
	let clientId: number = session.id_client;

	return data[clientId][lang][component];
}

export function set(component: string, res: any[], session): void {

	let lang = LANG[session.language];
	let clientId: number = session.id_client;

	if (typeof data[clientId] == "undefined") {
		data[clientId] = {};
		_.each(Object.values(LANG), langId => {
			data[clientId][langId] = {};
		})
	}

	data[clientId][lang][component] = {};
	_.each(res, (item) => {
		data[clientId][lang][component][item.id] = item;
	})
}

export function validate(session, ...components): string[] {
	let lang = LANG[session.language];
	let clientId: number = session.id_client;
	if (typeof data[clientId] === 'undefined') return components;
	if (typeof data[clientId][lang] === 'undefined') return components;

	let newComponents = [];

	for (let i = 0; i < components.length; i++) {
		if (typeof data[clientId][lang][components[i]] === 'undefined')
			newComponents.push(components[i]);
	}

	return newComponents;
}

export async function use(request, ...components): Promise<void> {
	components = validate(request.session.user, ...components)

	if (components.length === 0) return new Promise((res, rej) => res());

	await import('./../../components').then(componentsClasses => {
		let promises: Promise<any>[] = [];

		_.each(_.at(componentsClasses, ...components), (componentsClass) => {
			let c: {_get: CallableFunction}  = new componentsClass();
			promises.push(c._get({}, request));
		})

		return Promise.all(promises).then(responses => {
			_.each(responses, async (response, idx) => {
				set(components[idx], response['data'], request.session.user);
			})
		})
	}).catch(err => {
		if (CONFIG['debug']) console.log(err);
		throw ('caching_error')
	})
}

export function clear(...components): void {
	_.each(components, component => {
		delete data[component];
	})
}