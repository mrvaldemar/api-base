import { CONFIG } from '../../config';
import { ServerError } from '../../error';
import * as FileSys from 'fs';

export class Privileges {
	static validate = (session, component: string, action: string, params: any): boolean => {
		var role = 0;
		if (typeof params['key'] !== 'undefined') {
			session.user = CONFIG['secret'] === params['key'] ? params['session'] : undefined;
		}
		if (typeof session.user !== 'undefined') {
			role = session.user.role
		}
		var permisos = JSON.parse(FileSys.readFileSync(`${CONFIG['root']}/src/dependencies/privileges/privileges.json`, 'utf8'));

		if (typeof permisos[component] !== 'object') return false;
		if (typeof permisos[component][action] !== 'number') return false;

		return !!+(permisos[component][action] >> (role)).toString(2).slice(-1);
	}
}