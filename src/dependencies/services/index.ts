import { Privileges } from '..'
import { ServerError } from '../../error';
import * as modelos from '../../components';
import * as Utils from '../utils';
import { CONFIG } from '../../config';


export class Services {
	static getRequest = (request): object => {
		if (typeof request.body !== 'undefined') return request.body;
	}

	static execute = async (parametros: object, request) => {

		if (CONFIG['debug']) {
			console.log("\n" + parametros['component'] + '/' + parametros['action']);
		}
		let component = parametros['component'];

		if (!modelos[component]) {
			throw new ServerError(`component_doesnt_exist`, 404);
		}
		let instancia = new modelos[component]();

		let action = '_' + parametros['action'];
		if (!instancia[action]) {
			throw new ServerError(`action_doesnt_exit`, 404);
		}

		if (!Privileges.validate(request.session, component, parametros['action'], parametros)) {
			throw new ServerError(`not_authorized`, 401);
		}

		if (instancia[action] instanceof (async () => { }).constructor) {
			return await instancia[action](parametros, request);
		}

		return instancia[action](parametros, request);
	}
}